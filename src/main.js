import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import vSelect from 'vue-select'
import VueQrcodeReader from 'vue-qrcode-reader'
import VueSweetalert2 from 'vue-sweetalert2'
import Default from './components/layouts/Default.vue'
import Blank from './components/layouts/Blank.vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-select/dist/vue-select.css'

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueAxios, axios)
// Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VueQrcodeReader)
Vue.use(VueSweetalert2)

Vue.component('v-select', vSelect)
Vue.component('blank-layout', Blank)
Vue.component('default-layout', Default)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
