import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/pages/Invoice.vue'
import store from '../store/index'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: {
      requiresAuth: true
    },
    component: Home
  },
  {
    path: '/invoice',
    name: 'Invoice',
    redirect: '/'
  },
  {
    path: '/invoice/:id',
    name: 'InvoiceDetail',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/InvoiceDetail.vue')
  },
  {
    path: '/invoice/bill/:id',
    name: 'Billing',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/Billing.vue')
  },
  {
    path: '/addInvoice',
    name: 'AddInvoice',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/AddInvoice.vue')
  },
  {
    path: '/product',
    name: 'Product',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/pages/Product.vue')
  },
  {
    path: '/category',
    name: 'Category',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/pages/Category.vue')
  },
  {
    path: '/category/:id',
    name: 'CategoryDetail',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/CategoryDetail.vue')
  },
  {
    path: '/member',
    name: 'Member',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/pages/Member.vue')
  },
  {
    path: '/member/:id',
    name: 'MemberDetail',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/MemberDetail.vue')
  },
  {
    path: '/tier',
    name: 'Tier',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/pages/Tier.vue')
  },
  {
    path: '/tier/:id',
    name: 'TierDetail',
    meta: {
      requiresAuth: true
    },
    component: () => import('../views/other/TierDetail.vue')
  },
  {
    path: '/login',
    name: 'login',
    params: {},
    meta: { layout: 'blank' },
    component: () => import('../views/auth/Login.vue')
  },
  {
    path: '/login/:username',
    name: 'login-username',
    meta: { layout: 'blank' },
    component: () => import('../views/auth/Login.vue')
  },
  {
    path: '/register',
    name: 'register',
    meta: { layout: 'blank' },
    component: () => import('../views/auth/Register.vue')
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'notfound',
    meta: { layout: 'blank' },
    component: () => import('../views/other/404.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      to.meta.user = store.getters.authUser
      // console.log('to.meta', to.meta)
      next()
      return
    }
    next({ path: '/login', name: 'login', meta: { layout: 'blank' } })
  } else {
    next()
  }
})

export default router
