import Api from './Api'
export default {
  getAll () {
    return Api.get('members')
  },
  getById (id) {
    return Api.get(`members/${id}`)
  },
  getInvoices (id) {
    return Api.get(`members/${id}/invoices`)
  },
  add (body) {
    return Api.post('members', body)
  },
  update (body) {
    return Api.put(`members/${body._id}`, body)
  },
  changeTier (body) {
    return Api.put(`members/${body.id}/changetier`, body)
  },
  changePin (body) {
    return Api.put(`members/${body.id}/changepin`, body.pin)
  },
  delete (id) {
    return Api.delete(`members/${id}`)
  },
  generateQR (id) {
    return Api.post(`members/${id}/generate`)
  }
}
