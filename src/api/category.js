import Api from './Api'
export default {
  getAll () {
    return Api.get('categories')
  },
  getById (id) {
    return Api.get(`categories/${id}`)
  },
  getProducts (id) {
    return Api.get(`categories/${id}/products`)
  },
  add (body) {
    return Api.post('categories', body)
  },
  update (body) {
    return Api.put(`categories/${body._id}`, body)
  },
  delete (id) {
    return Api.delete(`categories/${id}`)
  }
}
