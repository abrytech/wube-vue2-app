import Api from './Api'
export default {
  getAll () {
    return Api.get('tiers')
  },
  getById (id) {
    return Api.get(`tiers/${id}`)
  },
  getMembers (id) {
    return Api.get(`tiers/${id}/members`)
  },
  add (body) {
    return Api.post('tiers', body)
  },
  update (body) {
    return Api.put(`tiers/${body._id}`, body)
  },
  delete (id) {
    return Api.delete(`tiers/${id}`)
  }
}
