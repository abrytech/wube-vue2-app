export default (status) => {
  switch (status) {
    case 401:
      return '401 Unauthorized'
    case 403:
      return '403 Forbidden'
    case 404:
      return '404 Not Found'
    case 408:
      return '408 Request Timeout'
    case 500:
      return '500 Internal Server Error'
    case 502:
      return ' 502 Bad Gateway'
    case 503:
      return ' 503 Service Unavailable'
    default:
      return ''
  }
}
