import axios from 'axios'
const Api = axios.create({
  baseURL: 'http://localhost:9090/'
  // baseURL: 'http://192.168.137.1:9090/'
})

const token = window.localStorage.getItem('token')

if (token) {
  Api.defaults.headers.common.Authorization = `Bearer ${token}`
}

export default Api
