import Api from './Api'
export default {
  getAll () {
    return Api.get('invoices')
  },
  getById (id) {
    return Api.get(`invoices/${id}`)
  },
  add (body) {
    return Api.post('invoices', body)
  },
  checkpin (body) {
    return Api.post(`invoices/${body.id}/checkpin`, body)
  },
  update (body) {
    return Api.put(`invoices/${body._id}`, body)
  },
  payBill (body) {
    return Api.put(`invoices/${body._id}/complete`, body)
  },
  delete (id) {
    return Api.delete(`invoices/${id}`)
  }
}
