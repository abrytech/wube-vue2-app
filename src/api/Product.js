import Api from './Api'
export default {
  getAll () {
    return Api.get('products')
  },
  getById (id) {
    return Api.get(`products/${id}`)
  },
  add (body) {
    return Api.post('products', body)
  },
  update (body) {
    return Api.put(`products/${body._id}`, body)
  },
  upload (body) {
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return Api.post('products/import', body, config)
  },
  delete (id) {
    return Api.delete(`products/${id}`)
  }
}
