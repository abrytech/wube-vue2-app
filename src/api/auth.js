// import
import Api from './Api'
export default {
  async login (body) {
    try {
      return Api.post('auth/login', body)
    } catch (err) {
      return console.log(err)
    }
  },
  async register (body) {
    try {
      return Api.post('auth/register', body)
    } catch (err) {
      return console.log(err)
    }
  }
}
