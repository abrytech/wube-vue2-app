import Vue from 'vue'
import Vuex from 'vuex'

import tierModule from './module/tier'
import productModule from './module/product'
import memberModule from './module/member'
import invoiceModule from './module/invoice'
import authModule from './module/auth'
import categoryModule from './module/category'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    errors: []
  },
  mutations: {
    SET_ERROR (state, payload) { state.errors.push(payload) },
    CLEAR_ERROR (state, payload) { state.errors = payload }
  },
  actions: {
    clearError ({ commit }) {
      commit('CLEAR_ERROR', [])
    }
  },
  getters: {
    errors: state => state.errors
  },
  modules: {
    tierModule,
    productModule,
    memberModule,
    invoiceModule,
    authModule,
    categoryModule
  }
})
