import Category from '../../api/category'
const categoryModule = {
  // namespaced: true,
  state: {
    status: '',
    error: '',
    category: {},
    categories: [],
    products: []
  },
  mutations: {
    SET_CATEGORY (state, payload) {
      state.category = payload
      state.categories.push(payload)
      state.status = 'success'
    },
    SET_CATEGORY_PRODUCTS (state, payload) {
      state.products = payload
      state.status = 'success'
    },
    SET_CATEGORY_ERROR (state, payload) {
      state.error = payload
      state.status = 'error'
    },
    CLEAR_CATEGORY_ERROR (state) {
      state.error = ''
      state.status = ''
    },
    SET_CATEGORY_STATUS (state, payload) {
      state.status = payload
    },
    UPDATE_CATEGORY (state, payload) {
      state.category = payload
      state.categories = state.categories.map(category => {
        if (payload._id === category._id) return payload
        else return category
      })
      state.status = 'success'
    },
    DELETE_CATEGORY (state, id) {
      state.categories = state.categories.filter(category => category._id !== id)
      state.status = 'success'
    },
    SET_ALL_CATEGORY (state, categories) {
      state.categories = categories
      state.status = 'success'
    }
  },
  actions: {
    async addCategory ({ commit }, body) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.add(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_CATEGORY', value.data.category)
      }).catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    },
    async updateCategory ({ commit }, body) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.update(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_CATEGORY', value.data.category)
      }).catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    },
    async getAllCategory ({ commit }) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.getAll().then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_ALL_CATEGORY', value.data.categories)
      })
        .catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    },
    async getCategory ({ commit }, id) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.getById(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_CATEGORY', value.data.category)
      }).catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    },
    async getCategoryProducts ({ commit }, id) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.getProducts(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_CATEGORY_PRODUCTS', value.data.products)
      }).catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    },
    async deleteCategory ({ commit }, id) {
      commit('SET_CATEGORY_STATUS', 'loading')
      Category.delete(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_CATEGORY_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('DELETE_CATEGORY', id)
      }).catch(e => { commit('SET_CATEGORY_ERROR', e.toString()) })
    }
  },
  getters: {
    categories: state => state.categories,
    category: state => state.category,
    categoryStatus: state => state.status,
    categoryProducts: state => state.products,
    categoryError: state => state.error
  }
}
export default categoryModule
