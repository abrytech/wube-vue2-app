import Member from '../../api/Member'
const memberModule = {
  // namespaced: true,
  state: {
    status: '',
    error: '',
    member: {},
    members: [],
    invoices: [],
    memberSearch: []
  },
  mutations: {
    SET_MEMBER (state, payload) {
      state.member = payload
      state.members.push(payload)
      state.status = 'success'
    },
    SET_MEMBER_INOICES (state, payload) {
      state.invoices = payload
      state.status = 'success'
    },
    SET_MEMBER_ERROR (state, payload) {
      state.error = payload
      state.status = 'error'
    },
    CLEAR_MEMBER_ERROR (state) {
      state.error = ''
      state.status = ''
    },
    SET_MEMBER_STATUS (state, payload) {
      state.status = payload
    },
    UPDATE_MEMBER (state, payload) {
      state.member = payload
      state.members = state.members.map(member => {
        if (payload._id === member._id) return payload
        else return member
      })
      state.status = 'success'
    },
    DELETE_MEMBER (state, id) {
      state.members = state.members.filter(member => member._id !== id)
      state.status = 'success'
    },
    SET_ALL_MEMBER (state, members) {
      state.members = members
      state.status = 'success'
    }
  },
  actions: {
    async addMember ({ commit }, body) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.add(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_MEMBER', value.data.member)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async updateMember ({ commit }, body) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.update(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_MEMBER', value.data.member)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async getAllMember ({ commit }) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.getAll().then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_ALL_MEMBER', value.data.members)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async getMember ({ commit }, id) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.getById(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_MEMBER', value.data.member)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async getMemberInvoices ({ commit }, id) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.getInvoices(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_MEMBER_INOICES', value.data.invoices)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async onGenerateQR ({ commit }, id) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.generateQR(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_MEMBER_STATUS', value.data.msg)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    },
    async deleteMember ({ commit }, id) {
      commit('SET_MEMBER_STATUS', 'loading')
      Member.delete(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_MEMBER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('DELETE_MEMBER', id)
      }).catch(e => { commit('SET_MEMBER_ERROR', e.toString()) })
    }
  },
  getters: {
    members: state => state.members.map((m) => {
      if (m) {
        if (m.balance > 0 && m.balance < (m.tier.cost * m.tier.discount / 100)) {
          m._rowVariant = 'warning'
          m.balance_level = 'low'
        } else if (m.balance < 0) {
          m._rowVariant = 'danger'
          m.balance_level = 'debt'
        }
        return m
      }
      return m
    }),
    member: state => state.member,
    memberStatus: state => state.status,
    memberError: state => state.error,
    memberInvoices: state => state.invoices
  }
}
export default memberModule
