import Product from '../../api/Product'
const productModule = {
  // namespaced: true,
  state: {
    status: '',
    error: '',
    product: {},
    products: [],
    productSearch: []
  },
  mutations: {
    SET_PRODUCT (state, payload) {
      state.product = payload
      state.products.push(payload)
      state.status = 'success'
    },
    SET_PRODUCT_ERROR (state, payload) {
      state.error = payload
      state.status = 'error'
    },
    CLEAR_PRODUCT_ERROR (state) {
      state.error = ''
      state.status = ''
    },
    SET_PRODUCT_STATUS (state, payload) {
      state.status = payload
    },
    UPDATE_PRODUCT (state, payload) {
      state.product = payload
      state.products = state.products.map(product => {
        if (payload._id === product._id) return payload
        else return product
      })
      state.status = 'success'
      // if (state.status == 'success') {}
    },
    DELETE_PRODUCT (state, id) {
      state.products = state.products.filter(product => product._id !== id)
      state.status = 'success'
    },
    SET_ALL_PRODUCT (state, products) {
      state.products = products
      state.status = 'success'
    }
  },
  actions: {
    async addProduct ({ commit }, body) {
      commit('SET_PRODUCT_STATUS', 'loading')
      Product.add(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_PRODUCT_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_PRODUCT', value.data.product)
      }).catch(e => { commit('SET_PRODUCT_ERROR', e.toString()) })
    },
    async updateProduct ({ commit }, body) {
      commit('SET_PRODUCT_STATUS', 'loading')
      Product.update(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_PRODUCT_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_PRODUCT', value.data.product)
      }).catch(e => { commit('SET_PRODUCT_ERROR', e.toString()) })
    },
    async getAllProduct ({ commit }) {
      commit('SET_PRODUCT_STATUS', 'loading')
      Product.getAll().then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_PRODUCT_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_ALL_PRODUCT', value.data.products)
      })
        .catch(e => { commit('SET_PRODUCT_ERROR', e.toString()) })
    },
    async getProduct ({ commit }, id) {
      commit('SET_PRODUCT_STATUS', 'loading')
      Product.getById(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_PRODUCT_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_PRODUCT', value.data.product)
      }).catch(e => { commit('SET_PRODUCT_ERROR', e.toString()) })
    },
    async deleteProduct ({ commit }, id) {
      commit('SET_PRODUCT_STATUS', 'loading')
      Product.delete(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_PRODUCT_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('DELETE_PRODUCT', id)
      }).catch(e => { commit('SET_PRODUCT_ERROR', e.toString()) })
    }
  },
  getters: {
    products: state => state.products,
    product: state => state.product,
    productStatus: state => state.status,
    productError: state => state.error
  }
}
export default productModule
