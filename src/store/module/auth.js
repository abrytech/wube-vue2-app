import Auth from '../../api/auth'
import Api from '../../api/Api'

const authModule = {
  state: {
    status: '',
    error: '',
    token: window.localStorage.getItem('token') || '',
    user: {
      _id: '',
      username: window.localStorage.getItem('username') || '',
      password: ''
    }
  },
  mutations: {
    AUTH_REQUEST (state) {
      state.status = 'loading'
    },
    AUTH_SUCCESS (state, data) {
      state.status = 'success'
      state.error = ''
      state.token = data.token
      state.user = data.user
      console.log('state.user:', state.user)
    },
    AUTH_ERROR (state, error) {
      state.status = 'error'
      state.error = error
      state.token = ''
      state.user = {
        _id: '',
        username: '',
        password: ''
      }
    },
    CLEAR_AUTH_ERROR (state) {
      state.error = ''
    },
    LOGOUT (state) {
      state.status = ''
      state.error = ''
      state.token = ''
      state.user = {
        _id: '',
        username: '',
        password: ''
      }
    }
  },
  actions: {
    login ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('AUTH_REQUEST')
        Auth.login(user)
          .then(resp => {
            const user = resp.data.user
            const token = resp.data.token
            const username = resp.data.user.username
            window.localStorage.setItem('token', token)
            window.localStorage.setItem('username', username)
            Api.defaults.headers.common.Authorization = `Bearer ${token}`
            console.log('user', user, 'token', token)
            commit('AUTH_SUCCESS', { token, user })
            resolve(resp)
          }).catch((err) => {
            console.log('err.status', err.status)
            window.localStorage.removeItem('token')
            window.localStorage.removeItem('username')
            commit('AUTH_ERROR', err)
            reject(err)
          })
      })
    },
    register ({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('AUTH_REQUEST')
        Auth.register(user)
          .then(resp => {
            const user = resp.data.user
            console.log(user)
            commit('AUTH_SUCCESS', { token: null, user })
            resolve(resp)
          })
          .catch(err => {
            commit('AUTH_ERROR', err)
            reject(err)
          })
      })
    },
    logout ({ commit }) {
      return new Promise((resolve, reject) => {
        commit('LOGOUT')
        window.localStorage.removeItem('token')
        window.localStorage.removeItem('username')
        delete Api.defaults.headers.common.Authorization
        resolve()
      })
    }
  },
  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    authUser: state => state.user,
    authError: state => state.error
  }
}
export default authModule
