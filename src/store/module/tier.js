import Tier from '../../api/Tier'
const tierModule = {
  // namespaced: true,
  state: {
    status: '',
    error: '',
    members: [],
    tier: {},
    tiers: []
  },
  mutations: {
    SET_TIER (state, payload) {
      state.tier = payload
      state.tiers.push(payload)
      state.status = 'success'
    },
    SET_TIER_ERROR (state, payload) {
      state.error = payload
      state.status = 'error'
    },
    CLEAR_TIER_ERROR (state) {
      state.error = ''
      state.status = ''
    },
    SET_TIER_STATUS (state, payload) {
      state.status = payload
    },
    SET_TIER_MEMBER (state, members) {
      state.members = members
      state.status = 'success'
    },
    UPDATE_TIER (state, payload) {
      state.tier = payload
      state.tiers = state.tiers.map(tier => {
        if (payload._id === tier._id) return payload
        else return tier
      })
      state.status = 'success'
    },
    DELETE_TIER (state, id) {
      state.tiers = state.tiers.filter(tier => tier._id !== id)
      state.status = 'success'
    },
    SET_ALL_TIER (state, tiers) {
      state.tiers = tiers
      state.status = 'success'
    }
  },
  actions: {
    async addTier ({ commit }, body) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.add(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_TIER', value.data.tier)
      }).catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    },
    async updateTier ({ commit }, body) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.update(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_TIER', value.data.tier)
      }).catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    },
    async getAllTier ({ commit }) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.getAll().then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_ALL_TIER', value.data.tiers)
      })
        .catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    },
    async getTierMembers ({ commit }, id) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.getMembers(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_TIER_MEMBER', value.data.members)
      }).catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    },
    async getTier ({ commit }, id) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.getById(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_TIER', value.data.tier)
      }).catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    },
    async deleteTier ({ commit }, id) {
      commit('SET_TIER_STATUS', 'loading')
      Tier.delete(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_TIER_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('DELETE_TIER', id)
      }).catch(e => { commit('SET_TIER_ERROR', e.toString()) })
    }
  },
  getters: {
    tiers: state => state.tiers,
    tierMembers: state => state.members,
    tier: state => state.tier,
    tierStatus: state => state.status,
    tierError: state => state.error
  }
}
export default tierModule
