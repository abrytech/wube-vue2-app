import Invoice from '../../api/Invoice'
const invoiceModule = {
  // namespaced: true,
  state: {
    status: '',
    error: '',
    invoice: {},
    invoices: []
  },
  mutations: {
    SET_INVOICE (state, payload) {
      state.invoice = payload
      state.invoices.push(state.invoice)
      state.status = 'success'
    },
    SET_INVOICE_ERROR (state, payload) {
      state.error = payload
      state.status = 'error'
    },
    CLEAR_INVOICE_ERROR (state) {
      state.error = ''
      state.status = ''
    },
    SET_INVOICE_STATUS (state, payload) {
      state.status = payload
    },
    UPDATE_INVOICE (state, payload) {
      state.invoice = payload
      state.invoices = state.invoices.map(invoice => {
        if (payload._id === invoice._id) return payload
        else return invoice
      })
      state.status = 'success'
    },
    DELETE_INVOICE (state, id) {
      state.invoices = state.invoices.filter(invoice => invoice._id !== id)
      state.status = 'success'
    },
    SET_ALL_INVOICE (state, invoices) {
      state.invoices = invoices
      state.status = 'success'
    }
  },
  actions: {
    async addInvoice ({ commit }, body) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.add(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_INVOICE', value.data.invoice)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async updateInvoice ({ commit }, body) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.update(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_INVOICE', value.data.invoice)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async billingInvoice ({ commit }, body) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.payBill(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('UPDATE_INVOICE', value.data.invoice)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async getAllInvoice ({ commit }) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.getAll().then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_ALL_INVOICE', value.data.invoices)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async getInvoice ({ commit }, id) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.getById(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_INVOICE', value.data.invoice)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async checkPin ({ commit }, body) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.checkpin(body).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('SET_INVOICE_STATUS', value.data)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    },
    async deleteInvoice ({ commit }, id) {
      commit('SET_INVOICE_STATUS', 'loading')
      Invoice.delete(id).then(value => {
        if (!!value.data.error || !!value.data.msg) commit('SET_INVOICE_ERROR', value.data.error ? value.data.error.message : value.data.msg)
        else commit('DELETE_INVOICE', id)
      }).catch(e => { commit('SET_INVOICE_ERROR', e.toString()) })
    }
  },
  getters: {
    invoices: state => {
      const invoices = state.invoices.map(inv => {
        inv._rowVariant = inv.completed ? 'success' : ''
        return inv
      })
      return invoices
    },
    invoice: state => {
      const invoice = state.invoice
      let discount = 0
      let gross = 0
      if (state.invoice.products) {
        if (state.invoice.products.length > 0) {
          for (let i = 0; i < state.invoice.products.length; i++) {
            // console.log('typeOf', typeof (state.invoice.member.tier.discount / 100), 'value', (state.invoice.member.tier))
            discount += state.invoice.products[i].product.cost * state.invoice.products[i].qty * (state.invoice.member.tier.discount / 100)
            gross += state.invoice.products[i].product.cost * state.invoice.products[i].qty
          }
          invoice.discount = discount
          invoice.gross = gross
          return invoice
        } else return invoice
      } else return invoice
    },
    invoiceStatus: state => state.status,
    invoiceError: state => state.error
  }
}
export default invoiceModule
